/*
 * llog-example 0.1.0
 * Lukas Kasticky <lukas@kasticky.me>
 * Example implementation for llog
 * 
 * COMPILE:
 *     make example
 *     make example-nodebug # nothing will be logged
 * 
 * USAGE:
 *     ./build/example
 *     ./build/example-nodebug
 */

/*
 * You may want to use llog in debug mode exclusively. For this only include
 * `llog.h` when `DEBUG` has been defined. Otherwise all library functions are
 * expanded to 'nothing'.
 */
#if DEBUG
#include "../src/llog.h"
#else
#define llog_set_buffer_size(sz)
#define llog_register_stream(stream)
#define llog_shutdown()
#define llog(lvl, format, ...)
#endif

int main(int argc, char **argv)
{
    /*
     * Register streams by creating `llog_stream`-structs. Everything will
     * be logged to `stdout` but only warnings and errors will be written to
     * `last.log`.
     */
#if DEBUG
    // increase buffer size, because why not
    llog_set_buffer_size(32768);

    // register stdout as llog stream
    struct llog_stream stream_stdout;
    stream_stdout.stream = stdout;
    stream_stdout.opts = LLOG_INFO | LLOG_FORMAT_ANSI;
    llog_register_stream(&stream_stdout);

    // register file stream
    FILE *fstream = fopen("last.log", "w");
    struct llog_stream stream_fstream;
    stream_fstream.stream = fstream;
    stream_fstream.opts = LLOG_WARN | LLOG_FORMAT_PLAIN;
    llog_register_stream(&stream_fstream);
#endif

    /*
     * Logging just works by providing a log level and the format string to be
     * logged.
     */
    llog(LLOG_INFO, "Hello, World!");
    llog(LLOG_WARN, "This could go south...");

    // Options can be changed dynamically
#if DEBUG
    stream_stdout.opts ^= LLOG_FORMAT_ANSI;
#endif

    llog(LLOG_FAIL, "Oh no! %i", 42);
    llog(LLOG_INFO, "Bye.");

    /*
     * Free memory
     */
    llog_shutdown();

    return 0;
}

# `llog`

Log formatted text to one or multiple streams (filestream, stdout, etc.). `llog` also supports colored text using ANSI escape sequences, but this can be enabled/disabled for each stream individually. Log levels can also be set per stream.

```plaintext
[ 2020-12-24T02:05:32Z ][ INFO ] Hello, World!
[ 2020-12-24T02:05:32Z ][ WARN ] This could go south...
[ 2020-12-24T02:05:32Z ][ FAIL ] Oh no! 42
[ 2020-12-24T02:05:32Z ][ INFO ] Bye.
```

See [`example/example.c`](example/example.c) for a possible implementation. Everything is well documented in [`src/llog.h`](src/llog.h) as well.

Check out the [release page](https://gitlab.com/lukasticky/llog/-/releases) for static and shared versions of the library or compile it yourself:

```
# Library (.a & .so file)
make

# Example
make example

# Example in 'release mode' (llog is not present)
make example-nodebug

# Clean
make clean
```

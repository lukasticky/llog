CC = gcc
CFLAGS = -Wall

release: src/llog.c
	mkdir -p ./build ./dist
	$(CC) $(CFLAGS) -O3 -fPIC -c $^ -o ./build/llog.o
	ar rcs ./dist/llog.a ./build/llog.o
	$(CC) -shared ./build/llog.o -o ./dist/llog.so

example: example/example.c src/llog.c
	mkdir -p ./build
	$(CC) $(CFLAGS) -D DEBUG $^ -o ./build/example

example-nodebug: example/example.c src/llog.c
	mkdir -p ./build
	$(CC) $(CFLAGS) $^ -o ./build/example-nodebug

clean:
	rm -rdf ./build ./dist

.PHONY: clean

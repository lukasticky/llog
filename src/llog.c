#include "llog.h"

#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

/* Global variables */
static llog_stream **m_streams;
static size_t m_streams_count = 0;
static size_t m_buf_sz = 8192;

/* Build formatted string */
static char *m_build_string(const unsigned char opts, const char *s)
{
    // allocate buffers
    char *s_time = malloc(sizeof "[ 1970-01-01T00:00:00Z ]");
    char *s_txt = malloc(m_buf_sz);
    if (s_time == NULL || s_txt == NULL || errno)
    {
        printf("LLOG FAILED TO ALLOCATAE MEMORY\n");
        return "\0";
    }
    char *cur = s_txt;
    const char *end = s_txt + m_buf_sz;

    // get time in ISO8601
    time_t now = time(NULL);
    if (now == -1 || errno)
    {
        printf("LLOG FAILED TO GET TIME\n");
        return "\0";
    }
    cur += strftime(cur, end - cur, "[ %Y-%m-%dT%H:%M:%SZ ]", gmtime(&now));

    // set ansi codes
    if (opts & 0xf)
    {
        switch (opts & 0xf0)
        {
        case LLOG_FAIL:
            cur += snprintf(cur, end - cur, "\033[31m"); /* red */
            break;
        case LLOG_WARN:
            cur += snprintf(cur, end - cur, "\033[33m"); /* yellow */
            break;
        case LLOG_OK:
            cur += snprintf(cur, end - cur, "\033[32m"); /* green */
            break;
        default:
            break;
        }
    }

    // write log level
    switch (opts & 0xf0)
    {
    case LLOG_FAIL:
        cur += snprintf(cur, end - cur, "[ FAIL ]");
        break;
    case LLOG_WARN:
        cur += snprintf(cur, end - cur, "[ WARN ]");
        break;
    case LLOG_OK:
        cur += snprintf(cur, end - cur, "[  OK  ]");
        break;
    default:
        cur += snprintf(cur, end - cur, "[ INFO ]");
        break;
    }

    // write formatted status text
    cur += snprintf(cur, end - cur, " %s\n", s);

    // reset ANSI codes
    if (opts & 0xf)
        cur += snprintf(cur, end - cur, "\033[0m");

    return s_txt;
}

/*
 * Public function definitions
 */

void llog_set_buffer_size(size_t sz)
{
    m_buf_sz = sz;
    return;
}

int llog_register_stream(llog_stream *stream)
{
    m_streams = realloc(m_streams, (m_streams_count + 1) * sizeof(llog_stream));
    if (m_streams == NULL || errno)
    {
        printf("LLOG FAILED TO ALLOCATAE MEMORY\n");
        return -1;
    }
    m_streams[m_streams_count++] = stream;
    return 0;
}

void llog_shutdown()
{
    free(m_streams);
    return;
}

int llog(const char lvl, const char *format, ...)
{
    char *s = malloc(m_buf_sz);
    if (s == NULL || errno)
    {
        printf("LLOG FAILED TO ALLOCATAE MEMORY\n");
        return -1;
    }

    va_list args;
    va_start(args, format);
    vsnprintf(s, m_buf_sz, format, args);
    va_end(args);

    for (size_t it = 0; it < m_streams_count; it++)
    {
        if ((lvl >> 4 & 0xf) >= (m_streams[it]->opts >> 4 & 0xf))
        {
            if (fprintf(m_streams[it]->stream,
                        m_build_string(lvl | (m_streams[it]->opts & 0xf),
                                       s)) == 0)
            {
                return -1;
            }
        }
    }
    return 0;
}

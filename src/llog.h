/*
 * llog 0.1.0
 * Lukas Kasticky <lukas@kasticky.me>
 * Log formatted text to one or multiple streams
 * 
 * USAGE:
 *     See `example.c` for a possible implementation
 *     1. Register a stream (e.g. stdout) with `llog_register_stream()`
 *     3. Start logging with `llog()`
 *     4. Free memory with `llog_shutdown()`
 */

#ifndef LLOG_H
#define LLOG_H 1

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>

/*
 * Use ANSI escape sequences for formatting
 */
#define LLOG_FORMAT_PLAIN 0x00
#define LLOG_FORMAT_ANSI  0x01

/*
 * Log verbosity (minimum log level)
 */
#define LLOG_INFO 0x00
#define LLOG_OK   0x10
#define LLOG_WARN 0x20
#define LLOG_FAIL 0x30

/*
 * Contains a file stream and options for log verbosity and formatting
 */
typedef struct llog_stream
{
    FILE *stream;
    unsigned char opts;
} llog_stream;

/*
 * Set buffer size; defaults to 8192.
 */
void llog_set_buffer_size(size_t sz);

/*
 * Register a new stream for logging.
 * On success `0` is returned.
 */
int llog_register_stream(llog_stream *stream);

/*
 * Free memory taken up by llog.
 */
void llog_shutdown();

/*
 * Log a string.
 * On success `0` is returned.
 */
int llog(const char lvl, const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif /* llog.h  */
